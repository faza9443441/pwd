<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="stylesheet" href="style.css">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Latihan 1</title>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
</head>
<body>
  <nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">Navbar</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="index.html">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="biodata.html">Biodata</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="galeri.html">Galeri</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="berita.html">Berita</a>
          </li>
        </form>
      </div>
    </div>
  </nav>


  <div class="row">
    <div class="col-md-3">
      <div class="card">
        <div class="card-body bg-primary">
          <img src="Gojo Pc wallpaper.png"  alt="gojo" class="img-fluid">
        </div>
      </div>
    </div>
    <div class="col-md-9">
      <div class="card">
        <div class="card-body bg-primary">
          <table class="table">
            <thead>
              <h1 class="text-center">BIODATA</h1>
            </thead>
            <tbody>
              <tr class="table-primary">
                <td class="table-primary">NPM</td>
                <td>:</td>
                <td>011220040</td>
              </tr>
              <tr>
                <td>Nama</td>
                <td>:</td>
                <td>Faza Al Frrisi</td>
              </tr>
              <tr class="table-primary">
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td>Pria</td>
              </tr>
              <tr>
                <td>tempat dan tanggal lahir</td>
                <td>:</td>
                <td>Palembang, 09-10-2002</td>
              </tr>
              <tr>
                <td class="table-primary">Alamat</td>
                <td class="table-primary">:</td>
                <td class="table-primary">Sumatra selatan. banyuasin. talang kelapa. Jalan Raya Palembang-Sekayu. Yayasan Akuis pusat/td>
                </tr>
                <tr>
                  <td>No.HP</td>
                  <td>:</td>
                  <td>085161091013</td>
                </tr>
                <tr>
                  <td class="table-primary">hobby</td>
                  <td class="table-primary">:</td>
                  <td class="table-primary">
                    <ul>
                      <li>otomotif</li>
                      <li>gadget</li>
                      <li>Game Magic Chess</li>
                    </ul>
                  </td>
                </tr>
                <tr>
                  <td>Sekolah</td>
                  <td>:</td>
                  <td>
                    <ol>
                      <li>SDIT Izzatuna</li>
                      <li>Ma'had Al Islamiy Aqulu El Muqoffa</li>
                    </ol>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
  </body>
  </html>